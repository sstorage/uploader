<?php

namespace App\Services;

use App\Exceptions\UnableToSaveFileException;
use App\Jobs\UpdateStatsAfterUpload;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\File;
use Exception;
use Tymon\JWTAuth\JWT;

class FileService
{
    public function store(UploadedFile $uploadFile, $userId = null): File
    {
        $uuid = Str::uuid();
        $serverFileName = $uuid . '.' . $uploadFile->extension();
        $path = $uploadFile->storeAs('files', $serverFileName, 's3');

        /** @var File $file */
        $file =  File::create([
            'uuid' => $uuid,
            'owner_id' => $userId,
            'name' => $uploadFile->getClientOriginalName(),
            'mime_type' => $uploadFile->getClientMimeType(),
            'path' => $path,
            'size' => $uploadFile->getSize(),
        ]);

        UpdateStatsAfterUpload::dispatch($file->toArray());

        return $file;
    }

    public function delete(File $file, $userId = null)
    {
        if ($file->owner_id != $userId) {
            throw new AuthorizationException('Access denied.', 403);
        }
        Storage::disk('s3')->delete($file->path);
        return $file->delete();
    }

    public function getStoreData(File $file): array
    {
        return [
            'uuid' => $file->uuid,
            //'file_uri' => $this->getFileUrl($file),
        ];
    }

    public function getFileUrl(File $file): string
    {
        return Storage::url($file->path);
    }
}
