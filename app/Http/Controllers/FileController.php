<?php

namespace App\Http\Controllers;

use App\Exceptions\UnableToSaveFileException;
use App\Http\Requests\FileStoreRequest;
use App\Models\File;
use App\Services\FileService;
use App\Services\JwtService;
use Illuminate\Http\Response;

class FileController extends Controller
{
    private FileService $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FileStoreRequest $request
     * @return Response
     * @throws UnableToSaveFileException
     */
    public function store(FileStoreRequest $request, JwtService $jwt): Response
    {
        $uploadFile = $request->file('file');
        $file = $this->service->store($uploadFile, $jwt->getUserId());

        return response()->sendSuccess($this->service->getStoreData($file));
    }

    public function delete(File $file, JwtService $jwt)
    {
        $this->service->delete($file, $jwt->getUserId());

        return response()->sendSuccess();
    }
}
